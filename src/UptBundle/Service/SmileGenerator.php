<?php

namespace UptBundle\Service;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class SmileGenerator {

    private $client;
    private $content;
    private $crawler;

    public function __construct() {

        $this->client = new Client();

        $this->content = $this->client->get('https://randstuff.ru/fact/')->getBody()->getContents();

        if (!empty($this->content)) $this->crawler = new Crawler($this->content);

    }

    public function getMessage() {

        return $this->crawler->filter('#fact td')->text();

    }

}
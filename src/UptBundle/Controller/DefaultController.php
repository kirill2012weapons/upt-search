<?php

namespace UptBundle\Controller;

use function GuzzleHttp\debug_resource;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UptBundle\Service\SmileGenerator as Smile;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="upt_home")
     */
    public function indexAction()
    {
        return $this->render('@UptViews/Default/index.html.twig', [
            'active' => [
                'home' => 'active',
                'getId' => '',
            ]
        ]);
    }

    /**
     * @Route("/get-id", name="upt_get_id")
     */
    public function getIdAction(
        Smile $smile
    )
    {
        $a = $smile->getMessage();

        dump($a);

        return $this->render('@UptViews/Default/get-id.html.twig', [
            'active' => [
                'home' => '',
                'getId' => 'active',
            ],
            'smileMessage' => $smile->getMessage(),
        ]);
    }
}
